#!/usr/bin/env python3

from PyPDF2 import PdfFileMerger
import imaplib
import email
import base64
import configparser
import datetime
import shutil
import os 
import sys

HERE = os.path.dirname(os.path.abspath(__file__))

def init_email(authfile):
    cfg = configparser.ConfigParser()
    cfg.read(authfile)
    user = cfg.get('email', 'user')
    passwd = cfg.get('email', 'pass')

    mail_client = imaplib.IMAP4_SSL('imap.gmail.com', 993)
    mail_client.login(user, passwd)

    mail_client.select('INBOX')
    return mail_client

def sendmail(authfile, email):
    from email.mime.multipart import MIMEMultipart
    from email.mime.text import MIMEText
    from email.mime.base import MIMEBase
    from email import encoders
    import smtplib

    cfg = configparser.ConfigParser()
    cfg.read(authfile)

    gmail_user = cfg.get('email', 'user')
    gmail_passwd = cfg.get('email', 'pass')
    recipient = email

    msg = MIMEMultipart()
    msg['From'] = gmail_user
    msg['Subject'] = 'Your Combined PDF'
    msg['To'] = recipient
    try:
        with open(HERE + '/jobs/result.pdf', 'rb') as fp:
            f = MIMEBase('application', "octet-stream")
            f.set_payload(fp.read())
        encoders.encode_base64(f)
        f.add_header('Content-Disposition', 'attachment', filename='RESULT.pdf')
        msg.attach(f)
    except:
        print("Unable to open one of the attachments. Error: ", sys.exc_info()[0])
        raise

    composed = msg.as_string()

    mail_server = smtplib.SMTP('smtp.gmail.com', 587)
    mail_server.ehlo()
    mail_server.starttls()
    mail_server.ehlo()
    mail_server.login(gmail_user, gmail_passwd)
    mail_server.sendmail(gmail_user, recipient, composed)
    mail_server.close()

def process_emails():
    authfile = HERE + '/pass'
    mail_client = init_email(authfile)

    result, data = mail_client.uid('search', None, "ALL")
    # search and return uids instead
    i = len(data[0].split()) # data[0] is a space separate string
    j = 1

    def is_in_db(time):
        dbfile = HERE + '/db.txt'
        if not os.path.isfile(dbfile):
            os.system('touch \'{}\''.format(dbfile))
        with open(dbfile, 'r+') as f:
            for line in f:
                if time in line:
                    return True
            else:
                f.write(time + '\n')
                return False

    # iterator from the latest email
    for x in range(i-1, 0, -1):
        if j == 10:
            break
        latest_email_uid = data[0].split()[x] # unique ids wrt label selected
        result, email_data = mail_client.uid('fetch', latest_email_uid, '(RFC822)')
        # fetch the email body (RFC822) for the given ID
        raw_email = email_data[0][1]
        raw_email_string = raw_email.decode('utf-8')
        email_message = email.message_from_string(raw_email_string)

        dl_dir = HERE + '/jobs/'
        if os.path.isdir(dl_dir):
            shutil.rmtree(dl_dir)

        os.makedirs(dl_dir)

        # email time format:
        # Thu, 19 Oct 2017 01:47:46 -0500 (CDT)
        time_split = email_message['date'].split(' ')
        if len(time_split) == 7:
            time_split = time_split[:-1]
            time = " ".join(time_split)
            time = datetime.datetime.strptime(time,
                    "%a, %d %b %Y %X %z")
        elif len(time_split) == 5:
            time = " ".join(time_split)
            time = datetime.datetime.strptime(time,
                    "%d %b %Y %X %z")
        elif len(time_split) == 6:
            time = " ".join(time_split)
            time = datetime.datetime.strptime(time,
                    "%a, %d %b %Y %X %z")

        files = []

        if 'PDFHELP' in email_message['subject'].upper():
            sender = email_message['from']
            print('Found email from {sender}'.format(sender=sender))
            if is_in_db(time.strftime("%s")):
                break
            for part in email_message.walk():
                if part.get_content_maintype() == 'multipart':
                    continue
                if part.get('Content-Disposition') is None: 
                    continue
                filename = dl_dir + part.get_filename()
                files.append(filename)
                fb = open(filename, 'wb')
                fb.write(part.get_payload(decode=True))
                fb.close()

            # Merge PDFs
            merger = PdfFileMerger()
            for f in files:
                merger.append(open(f, 'rb'))

            with open(HERE + '/jobs/result.pdf', 'wb') as fout:
                merger.write(fout)

            # Send merged PDF back to sender
            print('Sending email to {}'.format(sender))
            sendmail(authfile, sender)
            print('Sent email to {}'.format(sender))
        
        j += 1

    mail_client.close()
    mail_client.logout()

process_emails()
